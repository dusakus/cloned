/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dcode.games.uEngine2_cloneD.BGTasks.internalTasks;

import dcode.games.uEngine2_cloneD.BGTasks.PBGTask;
import dcode.games.uEngine2_cloneD.ResourceManager.DSU.DSU_NODE;

/**
 * @author dusakus
 */
public class DSU_OptimizeNode extends PBGTask {

    DSU_NODE dn;

    public DSU_OptimizeNode(DSU_NODE dn) {
        this.dn = dn;
        this.TaskPriority = PRIORITY_LOW;
    }

    @Override
    public boolean isReady() {
        return true;
    }

    @Override
    public void perform() {
        dn.optimize();
        done = true;
    }

}
