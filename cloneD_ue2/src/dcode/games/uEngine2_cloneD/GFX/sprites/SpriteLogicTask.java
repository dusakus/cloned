package dcode.games.uEngine2_cloneD.GFX.sprites;

/**
 * Created by dusakus on 29.03.15.
 */
public abstract class SpriteLogicTask {
    public abstract void update(SpriteWrapper s, int tickId);
}
