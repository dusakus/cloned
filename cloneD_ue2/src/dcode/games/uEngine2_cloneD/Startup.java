/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dcode.games.uEngine2_cloneD;

import dcode.games.uEngine2_cloneD.BGTasks.BackgroundTasks;
import dcode.games.uEngine2_cloneD.LOGIC.LogicTasks;
import dcode.games.uEngine2_cloneD.ResourceManager.ResMan;
import dcode.games.uEngine2_cloneD.SFX.tslib.TinySound;
import dcode.games.uEngine2_cloneD.tools.introPlayer;

import java.io.File;

/**
 * @author dusakus
 */
public class Startup {

    public static void StartGame(PuGameBase GB) {
        //System.setProperty("sun.java2d.opengl","True"); // does that change anything? (Except for crashing the game)
        Thread.currentThread().setName("ITNI");
        StData.GameInitializer = GB;
        StData.setup = GB.setup;
        StData.gameStorageDirectory = new File(System.getProperty("user.dir") + "/DCODE/uEngine2_cloneD/" + StData.setup.safeName);
        if (!StData.gameStorageDirectory.exists()) StData.gameStorageDirectory.mkdirs();
        StData.LOG = new DCoutputH(GB.setup.debug);
        StData.setup.loadSettings();
        StData.threadManager = new ThreadManager();
        StData.resources = new ResMan();
        StData.currentGC = new GameContainer();
        StData.logicTasks = new LogicTasks();
        StData.generalBGT = new BackgroundTasks();
        StData.threadManager.startEngine();
        StData.threadManager.setInputHandler(GB.initialInputHandler);
        if (!GB.setup.debug) new introPlayer().playIntro();
        try {
            GB.contentInitializer.loadInitialGameContent();
        } catch (Exception e) {
            StData.LOG.printerr(e, "GAME MIGHT BE BORKED!!!", "E3");
        }
        StData.threadManager.monitorThreads();
        TinySound.shutdown();
        StData.LOG.println("MONITOR THREAD CEASED BEING ALIVE, EXITTING");
        StData.LOG.dumpBuffer();
        StData.LOG.END("uEngine2_cloneD: BaYo!");

        try {
            Thread.sleep(100);
        } catch (InterruptedException ignored) {
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException ignored) {
        }
        GB.contentInitializer.engineStopped();
    }
}
